package td2;

public class Client implements Comparable<Client>{

	private String nom;
	private String penom;
	private double chiffreAffaires;
	private int num;
	
	public Client(){
		
	}
	
	public Client(String nom, String penom, double chiffreAffaires, int num) {
		super();
		this.nom = nom;
		this.penom = penom;
		this.setChiffreAffaires(chiffreAffaires);
		this.setNum(num);
	}

	public String toString(){
		return ("Num: " + this.num + " Nom:  " + this.nom + " Pr�nom:  " + this.penom 
				+ " CA:  " + (String.format("%.2f", this.chiffreAffaires) + " �" + "\n"));
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPenom() {
		return penom;
	}

	public void setPenom(String penom) {
		this.penom = penom;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		if(num <= 0){
			throw new IllegalArgumentException("Num imposible, saisissez une valeur mayor � cero");
		}
		this.num = num;
	}

	public double getChiffreAffaires() {
		return chiffreAffaires;
	}

	public void setChiffreAffaires(double chiffreAffaires) {
		if(chiffreAffaires <= 0){
			throw new IllegalArgumentException("Depot imposible, saisissez une valeur mayor � cero");
		}
		this.chiffreAffaires = chiffreAffaires;
	}


	@Override
	public int compareTo(Client client) {
		return Double.compare(client.getChiffreAffaires(), this.chiffreAffaires);
	}

}
