package td2;

public class ClientPriviligie extends Client{
    String bonClient;
    String clientExceptionel;
    String VIC;
    
    /**
     * M�thod pour trouver le type de client
     * @param client Re�oit l'objet de client
     * @return Le type de client
     */
	public String typeClient(Client client){
		if(client.getChiffreAffaires() > 1000){
			return ("bonClient");
		}else if(client.getChiffreAffaires() > 3000){
			return ("clientExceptionel");
		}else if(client.getChiffreAffaires() > 10000){
			return ("VIC");
		}else{
			return ("Le client: " + client.getNom() + " " + client.getPenom() +" "+ " ce n'est pas priviligie.");
		}
	}
	
	
}
