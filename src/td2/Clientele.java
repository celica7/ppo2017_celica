package td2;

import java.util.ArrayList;
import java.util.Collections;



public class Clientele extends ClientPriviligie{

	ArrayList<Client> clientes = new ArrayList<Client>();
	//ClientPriviligie clientPriviligie = new ClientPriviligie();
	
	public void addClient(Client client){
		clientes.add(client);
	}
	
	public void addCA(int num, double chiffre){
		if(num <= 0 || chiffre <= 0){
			throw new IllegalArgumentException("Depot imposible, saisissez une valeur mayor � cero");
		}
		double CA = this.clientes.get(num-2).getChiffreAffaires();
	    this.clientes.get(num-2).setChiffreAffaires(CA + chiffre);
	}
	
	
	public void afficherClientele(){
		if(clientes.isEmpty()){
			System.out.println("Il n'y a pas de divise");
		}else{
			
			Collections.sort(clientes);
			System.out.println(clientes.toString());
		}
	}
	
    public void faireAchat(Client client, double achat){
    	double reduction = 0;
    	double payer = 0;
    	
    	if(typeClient(client).equals("bonClient")){
    		reduction = (5 *achat)/100;
    	}else if(typeClient(client).equals("clientExceptionel")){
    		reduction = (15 *achat)/100;
    	}else if(typeClient(client).equals("VIC")){
    		reduction = (30 *achat)/100;
    	}
    	payer = achat - reduction;
		System.out.println("reducction: " + reduction + " �");
		System.out.println("A payer: " + String.format("%.2f", payer) + " �" + "\n");
    }
    
   public void tipoCliente(Client client){
	  String tipo =  typeClient(client);
	  System.out.println(client.getNom() + " "+ client.getPenom() + " " + "tipo: " + tipo);
    }
	
	public ArrayList<Client> getClientes() {
		return clientes;
	}

	public void setClientes(ArrayList<Client> clientes) {
		this.clientes = clientes;
	}
	
	
}
