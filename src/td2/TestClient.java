package td2;

public class TestClient {

	public static void main(String[] args){
	  try{
		// Objeto para acceder a los metodos
			Clientele clntl = new Clientele();
			
			//Se llena un been
			Client cliente1 = new Client("CABRERA", "Celica", 100.6,1);
			Client cliente2 = new Client("LOPEZ", "Pedro", 200.6,2);
			Client cliente3 = new Client("LOPEZ", "Carlos", 50.6,3);
			
			//Mute
			/*Client cliente = new ClientPriviligie();
			cliente.setNom("Bo");
			cliente.setPenom("Job");
			cliente.setChiffreAffaires(3000.3);
			cliente.setNum(4);*/
			
			//Add cliente
			clntl.addClient(cliente1);
			clntl.addClient(cliente2);
			clntl.addClient(cliente3);
			//clntl.addClient(cliente);
			//Type de client
			//clntl.tipoCliente(cliente);
			
			//Afficher liste
			clntl.afficherClientele();
			
			//Add CA au cliente 
			clntl.addCA(2, 800.3);
			
			//Afficher liste
			clntl.afficherClientele();
			
			//Type de client
			clntl.tipoCliente(cliente2);
			
			//Achat et Reduction selon le type de client
			clntl.faireAchat(cliente2, 200.3);
			
	  }catch(Exception e){
		  System.out.print("Ocurrio " + e);
	  }
	}

	
}
