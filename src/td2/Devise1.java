package td2;

import java.util.Comparator;

public class Devise1 implements Comparable<Devise1>{
  
	private String nomDevise;
	private double quantiteDevise;
	
	public Devise1(){
		
	}
	
	public Devise1(String nomDevise){
		   this.setNomDevise(nomDevise);
	}
	
	public Devise1(double montant){
	   this.setQuantiteDevise(montant);
	}
	
	public String toString(){
		return (this.nomDevise +"" + this.quantiteDevise);
	}
	
	public Devise1(String nomDevise, Double quantiteDevise) {
		this.setNomDevise(nomDevise);
		this.setQuantiteDevise(quantiteDevise);
	}
	
	/**
	 * M�thod qui retrait un montant
	 * @param montant  Re�oit une valeur de type double
	 */
	public  void retrait(double montant){
		if(montant <= 0){
			throw new IllegalArgumentException("Retrait imposible, saisissez une valeur mayor � cero");
		}else if(montant > getQuantiteDevise()){
			throw new IllegalArgumentException("Retrait imposible, pas assez d'argent");
		}else{
			double retrait = getQuantiteDevise() - montant;
			setQuantiteDevise(retrait);
		}
	}
	
	/**
	 * M�thod qui fait un depot
	 * @param nomDevise Nom de la devise. Re�oit une valeur de type String
	 * @param montant   Montant de la devise. Re�oit une valeur de type double
	 */
	public void depot( double montant){
		if(montant <= 0){
			throw new IllegalArgumentException("Depot imposible, Veuillez introducir un montant correct ");
		}else{
			this.quantiteDevise =+ montant;
		}
	}
	
	public String getNomDevise() {
		return nomDevise;
	}
	
	public void setNomDevise(String nomDevise) {
		this.nomDevise = nomDevise;
	}
	
	public double getQuantiteDevise() {
		return quantiteDevise;
	}
	
	public void setQuantiteDevise(double quantiteDevise) {
		if(quantiteDevise <= 0){
			throw new IllegalArgumentException("Veuillez introducir un montant correct");
		}else{
			this.quantiteDevise = quantiteDevise;
		}
	}

	@Override
	public int compareTo(Devise1 devise) {
		return Double.compare(this.quantiteDevise, devise.getQuantiteDevise());
	}
	
}
