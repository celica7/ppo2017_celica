package td2;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Portafeuille {
  
   ArrayList<Devise1> listeDevise = new ArrayList<Devise1>();
   
	/**
	 * M�thod por ajouter une devise
	 * @param nomDevise Nom de la devise. Re�oit une valeur de type String
	 * @param quantiteDevise Quantit� de la devise. Re�oit une valeur de type double
	 */
	public void ajouterDevise(String nomDevise, Double quantiteDevise){
		if(listeDevise.contains(nomDevise)){
			throw new IllegalArgumentException("La devise d�j� existe");
		}else{
			listeDevise.add(new Devise1(nomDevise,quantiteDevise));
		}
	}
	
	
	/**
	 * M�thod pour quiter une devise
	 * @param nomDevise Nom de la devise. Re�oit une valeur de type String
	 */
	public void quiteDevise(String nomDevise){
		if(listeDevise.contains(nomDevise)){
			listeDevise.remove(nomDevise);
		}else{
			throw new IllegalArgumentException("La devise ne existe pas");
		}
	}
	
	/**
	 * M�thod pour afficher la clientele
	 */
	public void afficherDevise(){
		if(listeDevise.isEmpty()){
			System.out.println("Il n'y a pas de divise");
		}else{
			Collections.sort(listeDevise);
			System.out.println(listeDevise.toString());
		}
	}


	public ArrayList<Devise1> getListeDevise() {
		return listeDevise;
	}


	public void setListeDevise(ArrayList<Devise1> listeDevise) {
		this.listeDevise = listeDevise;
	}
	
	
}
