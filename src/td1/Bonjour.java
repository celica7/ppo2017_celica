package td1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
public class Bonjour {
	/* Liste des classes et m�thodes utilis�es pour faire cet affichage
	 * Listes
	 * java.io.BufferedReader
	 * java.io.InputStreamReader
	 * java.util.Scanner
	 * 
	 * M�thodes
	 * nextLine();
	 * readLine();
	 */
	// M�thod principal
	public static void main(String[] args)throws IOException{
		boolean okPrenom = false;
		boolean okNom = false;
		boolean okAge = false;
		//Scanner sc = new Scanner(System.in);
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		String pr = null;
		String nm = null;
		String valeur;
		do{
		System.out.println("Pr�nom: ");
	    //pr = sc.nextLine();
		pr = sc.readLine();
		okPrenom = validerPrenom(pr);
		}while(!okPrenom);
		
		do{
		 System.out.println("Nom: ");
		// nm = sc.nextLine();
		 nm = sc.readLine();
		 okNom = validerNom(nm);
		 }while(!okNom);
		 
		do{
		
		 try{
		 System.out.println("Ann�e de naissance: ");
		 //entier = sc.nextInt();
	       valeur = sc.readLine() ;
		   saludar(pr,nm);
		   okAge = calculerAge(valeur);
		 }catch(Exception e){
		   System.out.println("Essayer d'�crire un entier");
		 }
		 
		 }while(!okAge);
	}
	
	/**
	 * M�thod pour saluer
	 * @param pr Re�oit le pr�nom
	 * @param nm Re�oit le nom
	 */
	static void saludar(String pr, String nm){
			System.out.println("BONJOUR !" + pr + " " + nm);
	}
	
	/**
	 * M�thod pour valider le pr�nom
	 * @param nm re�oit le pr�nom
	 * @return   false: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerPrenom(String pr){
		String letra = pr.substring(0, 1);
	    if(!letra.equals(letra.toUpperCase())){
	    	System.out.println("Commencez par majuscule");
	    	return false;
	    }else{
	    	return true;
	    }
	}
	
	/**
	 * M�thod pour valider le nom
	 * @param nm re�oit le nom
	 * @return   False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerNom(String nm){
	    if(!nm.equals(nm.toUpperCase())){
	    	System.out.println("Ecrivez en majuscule");
	    	return false;
	    }else{
	    	return true;
	    }
	}
	
	/**
	 * M�thod por calculer l'�ge
	 * @param valeur re�oit l'an�e de naissance, c'est un entier
	 * @return False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean calculerAge(int valeur){
		if(valeur > 0){
		int age = 0;
		age = (2017 - valeur);
		System.out.println("tu as " + age + " ans");
		return true;
		}else{
		return false;
		}
	}
    
	/**
	 * M�thod por calculer l'�ge
	 * @param valeur re�oit l'an�e de naissance, c'est un r�el
	 * @return False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean calculerAge(double valeur){
		if(valeur > 0){
		double age = 0;
		age = (2017 - valeur);
		System.out.println("tu as " + age + " ans");
		return true;
		}else{
		return false;
		}
	}
	
	/**
	 * M�thod por calculer l'�ge
	 * @param valeur re�oit l'an�e de naissance, c'est une cha�ne
	 * @return False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean calculerAge(String valeur){
		int anee = Integer.parseInt(valeur);
		if(anee > 0){
		double age = 0;
		age = (2017 - anee);
		System.out.println("tu as " + age + " ans");
		return true;
		}else{
		return false;
		}
	}
	
}
