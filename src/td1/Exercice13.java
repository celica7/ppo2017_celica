package td1;

public class Exercice13 {

	public static void main(String[] args){
	    	int tab [] = { 7,9,2,3,4,5,6,};
	    	int tabTrie [] = triBulles(tab);
	    	afficherTab(tabTrie);
	}
	 
	 /**
	  * M�thod pour trier la tableau
	  * @param tab re�oit la tableau � trier
	  */
	 public static int[] triBulles(int tab []){
		 boolean ok;
		 do{
	     ok = false;
		 for(int i=0; i<tab.length - 1; i++){
			 if(tab[i] > tab[i + 1]){
				 echanger(tab,i, i+1);
				 ok = true;
			 }
		 }
		 }while(ok);
		 
		 return tab;
	 }
	 
	 /**
	  * M�thod pour �changer les valeurs d'un tableau
	  * @param tab re�oit la tableau 
	  * @param a   re�oit le premier valeur
	  * @param b   re�oit le deuxi�me valeur
	  */
	 public static void echanger(int tab[], int a, int b){
		 int temporal = tab[a];
		 tab[a] = tab[b];
		 tab[b] = temporal;
	 }
	 
	 /**
	  * M�thod pour afficher la tableau
	  * @param tab2 re�oit la tableau � afficher
	  */
	  public static void afficherTab(int tab2 []){
	    	for(int i=0; i<tab2.length; i++){
				System.out.println(tab2[i]);
			}
	  }
}
