package td1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Exercice2 {

	public static void main(String[] args)throws IOException{
		Scanner sc = new Scanner(System.in);
		//BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		String pr = null;
		String nm = null;
	
		System.out.println("Pr�nom: ");
	    pr = sc.nextLine();
		//pr = sc.readLine();
		
		System.out.println("Nom: ");
		nm = sc.nextLine();
		//nm = sc.readLine();
		
		saludar(pr,nm);
	}
	
	/**
	 * M�thod pour saluer
	 * @param pr Re�oit le pr�nom
	 * @param nm Re�oit le nom
	 */
	static void saludar(String pr, String nm){
			System.out.println("BONJOUR !" + pr + " " + nm);
	}
	
	/*
	 * Quels sont les avantages et inconv�nients des deux solutions ? Pourquoi la seconde
     * est-elle si compliqu�e ?
     * 
     * Avec la classe Scanner on peut indiquer le type de date � lire, mais si on saisie 
     * autre type de donn�es, se montrera un erreur. 
     * 
     * Avec la classe BufferedReader et InputStreamReader on peut lire une cha�ne mais on
     * ne peut pas indiquer le type de donn�es � recevoir. Alors on dois faire la conversion
     * de cha�ne au type de donn�es dont nous avons besoin.
	 */
	
}
