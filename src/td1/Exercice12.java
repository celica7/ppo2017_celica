package td1;

public class Exercice12 {

    public static void main(String[] args){
    	int tab [] = { 1,2,3,4,5,6,};
    	int tab2 [] = copieTableau(tab);
    	afficherTab(tab2);
	}
    
    /**
     * M�thod pour copier un tableau
     * @param tab re�oit un tableau
     * @return le nouveau tableau
     */
    public static int[] copieTableau(int tab[]){
    	int tab2 [] = new int [tab.length];
		for(int i=0; i<tab.length; i++){
			tab2[i] = tab[i];
		}
		
		return tab2;
	}
    
    /**
     * M�thod pour afficher la tableau
     * @param tab2 re�oit la tableau � afficher
     */
    public static void afficherTab(int tab2 []){
    	for(int i=0; i<tab2.length; i++){
			System.out.println(tab2[i]);
		}
    }
}
