package td1;

import java.util.Scanner;

public class Saisie {

	/**
	 * M�thod qui lit un r�el
	 * @param reel: re�oit un r�el
	 * @return r�el: retours le r�el
	 */
	static double litReel(String message){
		double reel = -1;
		try{
		Scanner sc = new Scanner(System.in);
		System.out.println(message);
		reel = sc.nextDouble();
		}catch(Exception e){
			System.out.println("Ecrivez un r�el");
		}
		return reel;
	}
	
	/**
	 * M�thod qui lit un entier
	 * @param entier: re�oit un entier
	 * @return retours l'entier
	 */
	static int litEntier(String message){
		int entier = 0;
		try{
			Scanner sc = new Scanner(System.in);
			System.out.println(message);
			entier = sc.nextInt();
			}catch(Exception e){
				System.out.println("Ecrivez un entier");
			}
		return entier;
	}
	
	/**
	 * M�thod qui lit une chaine
	 * @param chaine re�oit une chaine
	 * @return retours la cha�ne
	 */
	static String litChaine(String message){
		String chaine = null;
		try{
			Scanner sc = new Scanner(System.in);
			System.out.println(message);
			chaine = sc.nextLine();
			}catch(Exception e){
				System.out.println("Ecrivez une chaine");
			}
		return chaine;
	}
}
