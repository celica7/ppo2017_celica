package td1;

public class Exercice11 {

	public static void main(String[] args){
		
		int tabRemplit [] = saisieTable();
		afficherTab(tabRemplit);
	}
	
	/**
	 * M�thod pour saisie une table
	 * @return tab: renvoie la table avec les �l�ments
	 */
	public static int[] saisieTable(){
		int taille = Saisie.litEntier("Veuillez ecrivez la quantit� d'�l�ments de la table: ");
		int tab [] = new int [taille];
		for(int i=0; i<tab.length; i++){
			int valeur = Saisie.litEntier("Valeur de la position: " + i);
			tab[i] = valeur;
		}
		return tab;
	}
	
	/**
     * M�thod pour afficher le tableau
     * @param tab2 re�oit le tableau � afficher
     */
    public static void afficherTab(int tab2 []){
    	for(int i=0; i<tab2.length; i++){
			System.out.println(tab2[i]);
		}
    }
}
