package td1;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class Exercice10 {

	public static void main(String[] args){
		int valeur = 0;
		boolean okAge = false;
		do{
			 try{
			  valeur = Saisie.litEntier("Veuillez ecrivez votre ann�e de naissances");
			  okAge = validerAnee(valeur);
			 }catch(Exception e){
			  System.out.println("Essayer d'�crire un entier");
			 }
	    }while(!okAge);
		
		LocalDate date = LocalDate.now();
		DateTimeFormatter dateCourant = DateTimeFormatter.ofPattern("dd/MM/YY");
		String date1 = date.format(dateCourant);
		System.out.println(date1);
		
		DateTimeFormatter dateCourant5 = DateTimeFormatter.ofPattern("YYYY");
		int anne = Integer.parseInt(date.format(dateCourant5));
		
		int age = anne - valeur;
		System.out.println("Tu as: " + age + " ans");
		
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("EEEE d MMMM YYYY");
		String date2 = date.format(formatter2);
		System.out.println(date2);
		
        
	}
	
	/**
	 * M�thod por calculer l'�ge
	 * @param valeur re�oit l'an�e de naissance, c'est un entier
	 * @return False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerAnee(int valeur){
		return valeur > 0? true : false;
	}
}
