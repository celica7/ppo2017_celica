package td1;

public class Exercice14 {

	public static void main(String[] args){
		int tab [] = { 7,9,2,3,4,5,6,};
		System.out.println("La position du �l�ment est: " + positionTable(tab,2));
	}
	
	/**
	 * M�thod qui renvoie la position d'un �l�ment de la table
	 * @param tab re�oit la table
	 * @param element re�oit l'�l�ment 
	 * @return posElement envoie la position du �l�ment
	 */
	public static int positionTable(int tab[], int element){
	      int posElement = 0;
	      
	      for(int i = 0; i<tab.length; i++){
	         if(tab[i] == element){
	        	posElement = i;
	         }
	      }
	      return posElement;
	}
}
	
