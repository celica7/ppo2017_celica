package td1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exercice4 {

	public static void main(String[] args)throws IOException{
		boolean okPrenom = false;
		boolean okNom = false;
		
		//Scanner sc = new Scanner(System.in);
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		String pr = null;
		String nm = null;
		
		do{
		System.out.println("Pr�nom: ");
	    //pr = sc.nextLine();
		pr = sc.readLine();
		okPrenom = validerPrenom(pr);
		}while(!okPrenom);
		
		do{
		 System.out.println("Nom: ");
		// nm = sc.nextLine();
		 nm = sc.readLine();
		 okNom = validerNom(nm);
		 }while(!okNom);
		 
		saludar(pr,nm);
	}
	
	/**
	 * M�thod pour saluer
	 * @param pr Re�oit le pr�nom
	 * @param nm Re�oit le nom
	 */
	static void saludar(String pr, String nm){
			System.out.println("BONJOUR !" + pr + " " + nm);
	}
	
	/**
	 * M�thod pour valider le pr�nom
	 * @param nm re�oit le pr�nom
	 * @return   false: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerPrenom(String pr){
		String letra = pr.substring(0, 1);
	    if(!letra.equals(letra.toUpperCase())){
	    	System.out.println("Commencez par majuscule");
	    	return false;
	    }else{
	    	return true;
	    }
	}
	
	/**
	 * M�thod pour valider le nom
	 * @param nm re�oit le nom
	 * @return   False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerNom(String nm){
	    if(!nm.equals(nm.toUpperCase())){
	    	System.out.println("Ecrivez en majuscule");
	    	return false;
	    }else{
	    	return true;
	    }
	}
	
}
