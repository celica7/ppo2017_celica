package td1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exercice9 {

	public static void main(String[] args)throws IOException{
		boolean okPrenom = false;
		boolean okNom = false;
		boolean okAge = false;
		
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		String pr = null;
		String nm = null;
		int valeur = 0;
		do{
		
		pr = Saisie.litChaine("Veuillez ecrivez votre pr�nom");
		okPrenom = validerPrenom(pr);
		}while(!okPrenom);
		
		do{
		 System.out.println("Nom: ");
		 nm = Saisie.litChaine("Veuillez ecrivez votre nom");
		 okNom = validerNom(nm);
		 }while(!okNom);
		 
		do{
		 try{
		  System.out.println("Ann�e de naissance: ");
		  valeur = Saisie.litEntier("veuillez ecrivez votre ann�e de naissances");
		  okAge = validerAnee(valeur);
		 }catch(Exception e){
		  System.out.println("Essayer d'�crire un entier");
		 }
		 
		}while(!okAge);
		
		saludar(pr,nm,valeur);
	}
	
	/**
	 * M�thod pour saluer
	 * @param pr Re�oit le pr�nom
	 * @param nm Re�oit le nom
	 * @param anne Re�oit l'an�e
	 */
	static void saludar(String pr, String nm, int anne){
		int age = (2017 - anne);
	    System.out.println("BONJOUR !" + pr + " " + nm + " tu as: " + age + " ans");
	}
	
	/**
	 * M�thod pour valider le pr�nom
	 * @param nm re�oit le pr�nom
	 * @return   false: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerPrenom(String pr){
		String letra = pr.substring(0, 1);
	    if(!letra.equals(letra.toUpperCase())){
	    	System.out.println("Commencez par majuscule");
	    	return false;
	    }else{
	    	return true;
	    }
	}
	
	/**
	 * M�thod pour valider le nom
	 * @param nm re�oit le nom
	 * @return   False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerNom(String nm){
	    if(!nm.equals(nm.toUpperCase())){
	    	System.out.println("Ecrivez en majuscule");
	    	return false;
	    }else{
	    	return true;
	    }
	}
	
	/**
	 * M�thod por calculer l'�ge
	 * @param valeur re�oit l'an�e de naissance, c'est un entier
	 * @return False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerAnee(int valeur){
		return valeur > 0? true : false;
	}
	
	
}
