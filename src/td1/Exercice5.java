package td1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exercice5 {
	
	
	public static void main(String[] args)throws IOException{
		boolean okPrenom = false;
		boolean okNom = false;
		
		//Scanner sc = new Scanner(System.in);
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		String pr = null;
		String nm = null;
		int valeur;
		do{
		System.out.println("Pr�nom: ");
	    //pr = sc.nextLine();
		pr = sc.readLine();
		okPrenom = validerPrenom(pr);
		}while(!okPrenom);
		
		do{
		 System.out.println("Nom: ");
		// nm = sc.nextLine();
		 nm = sc.readLine();
		 okNom = validerNom(nm);
		 }while(!okNom);
		 
		
		 System.out.println("Ann�e de naissance: ");
		 //M�thod pour la classe Scanner
		 //valeur = sc.nextInt();
		 //M�thod pour la classe BufferedReader
	     valeur = Integer.parseInt(sc.readLine()) ;
		 saludar(pr,nm);
		 calculerAge(valeur);
		 
	}
	
	/**
	 * M�thod pour saluer
	 * @param pr Re�oit le pr�nom
	 * @param nm Re�oit le nom
	 */
	static void saludar(String pr, String nm){
			System.out.println("BONJOUR !" + pr + " " + nm);
	}
	
	/**
	 * M�thod pour valider le pr�nom
	 * @param nm re�oit le pr�nom
	 * @return   false: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerPrenom(String pr){
		String letra = pr.substring(0, 1);
	    if(!letra.equals(letra.toUpperCase())){
	    	System.out.println("Commencez par majuscule");
	    	return false;
	    }else{
	    	return true;
	    }
	}
	
	/**
	 * M�thod pour valider le nom
	 * @param nm re�oit le nom
	 * @return   False: si c'est mauvais. True: si c'est bien.
	 */
	static boolean validerNom(String nm){
	    if(!nm.equals(nm.toUpperCase())){
	    	System.out.println("Ecrivez en majuscule");
	    	return false;
	    }else{
	    	return true;
	    }
	}
	
	/**
	 * M�thod por calculer l'�ge
	 * @param valeur re�oit l'an�e de naissance, c'est un entier
	 * @return False: si c'est mauvais. True: si c'est bien.
	 */
	static void calculerAge(int valeur){
		if(valeur > 0){
		int age = 0;
		age = (2017 - valeur);
		System.out.println("tu as " + age + " ans");
		}
	}
	
	/*
	 * Que se passe-t-il si on saisit une cha�ne ou un r�el au lieu d�un entier, dans chacune
     * des deux m�thodes ?
	 * Se montre une exception java.lang.NumberFormatException, car c'est autre type de don�es
	 */
}
