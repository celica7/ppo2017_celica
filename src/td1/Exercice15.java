package td1;

import java.util.Arrays;

public class Exercice15 {

	public static void main(String[] args){
		int tab1 [] = {9,1,2,3,4,5};
		int tab2 [] = new int[tab1.length];
		
		
		// Fonction qui copie le contenu du premier tableau dans un nouveau tableau
		System.arraycopy(tab1, 0, tab2, 0, tab1.length);
		afficherTab(tab2);
		// Fonction de tri de table.
		Arrays.sort(tab1);
		afficherTab(tab1);
		// Fonction qui renvoie la position d�un �el�ement de la table.
		System.out.println("La position est: " + Arrays.binarySearch(tab1, 3));
		
		
	}
	
	/**
     * M�thod pour afficher la tableau
     * @param tab2 re�oit la tableau � afficher
     */
    public static void afficherTab(int tab2 []){
    	for(int i=0; i<tab2.length; i++){
			System.out.println(tab2[i]);
		}
    }
}
