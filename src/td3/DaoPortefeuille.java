package td3;

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DaoPortefeuille implements InterfaceDAO{
    /*
     * Variables de la classe
     */
	private Connection con;
    private PreparedStatement pst;
    private boolean status;
    private Portefeuille portefeuille;
    
    //Singleton
    private static DaoPortefeuille instance;
    private DaoPortefeuille() {}
    public static DaoPortefeuille getInstance() {
    if (instance==null) {
    instance = new DaoPortefeuille();
    }
    return instance;
    }
    
	@Override
	public boolean add(Object object) {
		portefeuille = (Portefeuille) object;
		try {
            con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute addPortefeuille ?;");
            pst.setString(1, portefeuille.getNom());
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur: " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Erreur: " + e);
            }
        }
        return status;
	}
	
	@Override
	public boolean update(Object object) {
		portefeuille = (Portefeuille) object;
		try {
			con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute updatePortefeuille ?;");
            pst.setString(1, portefeuille.getNom());
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur: " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (SQLException e) {
                System.out.println("Erreur: " + e.getMessage());
            }
        }

        return status;
	}
	
	@Override
	public boolean delete(Object object) {
		portefeuille = (Portefeuille) object;
		try {
			con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute deletePortefeuille ?;");
            pst.setString(1, portefeuille.getNom());
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur delete: " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (SQLException e) {
                System.out.println("Erreur: " + e.getMessage());
            }
        }
        return status;
	}
	
	@Override
	public List filterDataList(Object object) {
	return null;
	}
}
