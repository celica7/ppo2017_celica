package td3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Fichiersplats {
	
	public static void sauvegarder(List<Portefeuille> listePortefeuille){
		String pathAbsolute;
		//Dosier ou se trouve le sauvegarde
		File root = new File(".");
		FileWriter sauvegarde;
		PrintWriter escritor = null;
		
		try {
		    pathAbsolute = root.getCanonicalPath();
		    sauvegarde = new FileWriter(pathAbsolute + "/sauvegarde.txt");
		    escritor = new PrintWriter(new BufferedWriter(sauvegarde));
		    for(Portefeuille portefeuille : listePortefeuille){
		    	//Nom de portefeuille
		    	escritor.println("PORTEFEUILLE "+portefeuille.getNom());
		    	//On affiche les devises
		    	Iterator it = portefeuille.hmap.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry e = (Map.Entry)it.next();
			        escritor.println(e.getKey() + " : " + e.getValue());
			   }
		    	escritor.println("FIN PORTEFEUILLE "+portefeuille.getNom());
		    	escritor.println("");
		    }
		    System.out.println("Ficher cree dans: " + pathAbsolute);
		} catch (IOException ioe) {
		    ioe.printStackTrace();
		}  finally {
			try {
				if (escritor != null) {
					escritor.close();
				}
			} catch(Exception e){
				System.err.println(e.getMessage());
			}
		}
	}
	
}
