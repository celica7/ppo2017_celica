package td3;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {


	public static void main(String[] args) {
		/*
		 * Variables de la classe
		 */
		Scanner sc = new Scanner(System.in);
		int option = 0;
		String nomPortefeuille;
		List<Portefeuille> listePortefeuille = new ArrayList<>();
		
		
		
		if(Serealisation.getBackup() != null){
			listePortefeuille = Serealisation.getBackup();
		}
        /*
         * Menu de la classe 
         */
		do{
			System.out.println("===Menu===");
			System.out.println("1. Ajouter portefeuille");
			System.out.println("2. G�rer portefeuille");
			System.out.println("3. Suprimer portefeuille");
			System.out.println("4. Sortir");
			System.out.print("Option : ");
			option = sc.nextInt();
		
		    
		    switch(option){
		    case 1:
		    	//Variables de submenu 1
		    	int optionDevise = 0;
		    	String nDevise = "";
		    	double vDevise = 0;
		    	//Option pour ajouter un portefeuille
		    	System.out.println("==Ajouter portefeuille==");
		    	System.out.print("Nom de portefeuille : ");
		    	nomPortefeuille = sc.next();
		    	Portefeuille port = new Portefeuille(nomPortefeuille);
		    	//On ajout � la liste le nouveau portefeuille
		        listePortefeuille.add(new Portefeuille(port.getNom()));
		        DaoPortefeuille.getInstance().add(port);
		        //Submenu pour ajouter la devise
		        do{
		        	 System.out.println("Ajouter une devise : ");
				        System.out.println("1. Oui");
				        System.out.println("2. Non");
				        System.out.print("Option : ");
				        optionDevise = sc.nextInt();
				        switch(optionDevise){
				        case 1:
				        	System.out.print("Veuillez saisir le nom de la devise � ajouter :");
					    	nDevise = sc.next();
					    	System.out.print("Veuillez saisir le montant de la devise :");
					    	vDevise = sc.nextDouble();
				        	listePortefeuille.get(listePortefeuille.size()-1).addCartera(nDevise, vDevise);
				        	Devise dev = new Devise(nDevise);
				        	DaoDevise.getInstance().add(dev);
				        	break;
				        case 2:
				        	System.out.println("Retour :");
				        	break;
				        default:
				        	System.out.println("==Essayez de choisir une option du menu==");
				        	break;
		                }
		        }while(optionDevise != 2);
		    	break;
		    case 2:
		    	//Option pour g�rer le portefeuille
		    	
		    	int nPortefeuille = 0;
		    	int menuDevise = 0;
		    	String nomDevise = "";
		    	double valeurDevise = 0;
		    	
		    	System.out.println("==G�rer portefeuille==");
		    	//On v�rifie si c'est vide ou pas
		    	if(listePortefeuille.isEmpty()){
		    		System.out.println("Il n'y a pas portefeuille");
		    	}else{
		    		System.out.println("Choisissez le num�ro du portefeuille");
		    		//On affiche tous les portefeuilles
			    	for (int i = 0; i < listePortefeuille.size(); i++) {
			    		System.out.println((i+1)+" "+listePortefeuille.get(i));
					}
			    	System.out.print("Option : ");
			    	nPortefeuille = sc.nextInt();
			    	//Montrer le portefeuille
			    	System.out.println(listePortefeuille.get(nPortefeuille-1));
			    	//Montrer les devises
			    	listePortefeuille.get(nPortefeuille-1).montrerDevises();
			    	//Submenu pour g�rer les devises
			    	System.out.println("==G�rer devises==");
			    	System.out.println("1. Ajouter une devise");
			    	System.out.println("2. Ajouter un montant");
			    	System.out.println("3. Retrait un montant");
			    	System.out.println("4. Retour");
			    	System.out.print("Option : ");
			    	menuDevise = sc.nextInt();
			    	
			    	switch(menuDevise){
			    	case 1:
			    		System.out.print("Veuillez saisir le nom de la devise � ajouter :");
				    	nomDevise = sc.next();
				    	System.out.print("Veuillez saisir le montant de la devise :");
				    	valeurDevise = sc.nextDouble();
			    		listePortefeuille.get(nPortefeuille-1).addCartera(nomDevise, valeurDevise);
			    		Devise dev = new Devise(nomDevise);
			        	DaoDevise.getInstance().add(dev);
			        	//Bdd Contenu add montant
			        	Portefeuille porte = new Portefeuille();
			    		porte.setNom(listePortefeuille.get(nPortefeuille-1).getNom());
			        	DaoContenu.getInstance().addContenu(porte, dev, valeurDevise);
			    		break;
			    	case 2:
			    		try{
			    			System.out.print("Veuillez saisir le nom � laquelle vous voulez ajouter le montant :");
					    	nomDevise = sc.next();
					    	System.out.print("Veuillez saisir le montant que vous voulez ajouter :");
					    	vDevise = sc.nextDouble();
				    		listePortefeuille.get(nPortefeuille-1).ajouterMontant(nomDevise,vDevise);
				    		//Bdd Contenu ajout
				    		Portefeuille p = new Portefeuille();
				    		p.setNom(listePortefeuille.get(nPortefeuille-1).getNom());
				    		Devise d = new Devise(nomDevise);
				    		DaoContenu.getInstance().addMontant(p,d , vDevise);
			    		}catch(Exception e){
			    			System.out.println(e.getMessage()+ "V�rifiez le nom de la devise.");
			    		}
			    		break;
			    	case 3:
			    		try{
			    			System.out.print("Veuillez saisir le nom � laquelle vous voulez retrait le montant :");
					    	nomDevise = sc.next();
					    	System.out.print("Veuillez saisir le retrait :");
					    	vDevise = sc.nextDouble();
				    		listePortefeuille.get(nPortefeuille-1).retraitMontant(nomDevise,vDevise);
				    		//Bdd Contenu retrait
				    		Portefeuille po = new Portefeuille();
				    		po.setNom(listePortefeuille.get(nPortefeuille-1).getNom());
				    		Devise de = new Devise(nomDevise);
				    		DaoContenu.getInstance().addMontant(po,de, vDevise);
			    		}catch(Exception e){
			    			System.out.println(e.getMessage() + "V�rifiez le nom de la devise.");
			    		}
			    		break;
			    	case 4:
			    		System.out.println("Retour :");
			    		break;
			    	}
			    	
		    	}
		    	
		    	break;
		    case 3:
		    	System.out.println("==Suprimer portefeuille==");
		    	System.out.println("Choisissez le num�ro du portefeuille");
	    		//On affiche tous les portefeuilles
		    	for (int i = 0; i < listePortefeuille.size(); i++) {
		    		System.out.println((i+1)+" "+listePortefeuille.get(i));
				}
		    	System.out.print("Option : ");
		    	nPortefeuille = sc.nextInt();
		    	listePortefeuille.remove(nPortefeuille-1);
		    	break;
		    case 4:
		    	int nSortir = 0;
		    	System.out.println("==Sortir==");
		    	System.out.println("Avant de sortir voulez-vous sauvegarder la modification des portefeuilles et des devises ?");
		    	System.out.println("1. Oui");
		    	System.out.println("2. Non");
		    	System.out.print("Option: ");
		    	nSortir = sc.nextInt();
		    	
		    	switch(nSortir){
		    	case 1:
		    		//Sauvegarder
		    		Fichiersplats.sauvegarder(listePortefeuille);
		    		break;
		    	case 2:
		    		System.out.println("==Au revoir==");
		    		break;
		    	default:
		    		System.out.println("==Essayez de choisir une option du menu==");
		    		break;
		    	}
		    	break;
		    default:
		    	System.out.println("==Essayez de choisir une option du menu==");
		    	break;
		    }
		}while(option != 4);
		
		
		
	}


	
	
	
	
	
	

	
	
}
