package td3;

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DaoDevise implements InterfaceDAO{
	private Connection con;
    private ResultSet rs;
    private PreparedStatement pst;
    private boolean status;
    private Devise devise;
    
  //Singleton
    private static DaoDevise instance;
    private DaoDevise() {}
    public static DaoDevise getInstance() {
    if (instance==null) {
    instance = new DaoDevise();
    }
    return instance;
    }
    
    
	@Override
	public boolean add(Object object) {
		devise = (Devise) object;
		try {
            con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute addDevise ?;");
            pst.setString(1, devise.getNomDevise());
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur: " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Erreur: " + e);
            }
        }
        return status;
	}

	@Override
	public boolean update(Object object) {
		devise = (Devise) object;
		try {
			con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute updateDevise ?;");
            pst.setString(1, devise.getNomDevise());
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur: " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (SQLException e) {
                System.out.println("Erreur: " + e.getMessage());
            }
        }
        return status;
	}

	@Override
	public boolean delete(Object object) {
		devise = (Devise) object;
		try {
			con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute deleteDevise ?;");
            pst.setString(1, devise.getNomDevise());
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur delete: " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (SQLException e) {
                System.out.println("Erreur: " + e.getMessage());
            }
        }
        return status;
	}

	@Override
	public List filterDataList(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

}
