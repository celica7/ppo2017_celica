package td3;



public class Devise implements Comparable<Devise>{
    /**
     * Atributs de la classe
     */
	private String nomDevise;
	
	
	/**
	 * Constructeurs
	 */
	public Devise(){
		this("Euro");
	}
	
		
	public Devise(String nomDevise){
		this.setNomDevise(nomDevise);
		
	}
	
	public Devise(Devise d){
		this.nomDevise = d.nomDevise;
		
	}
	
	public boolean equals(Object d){
		if(d instanceof Devise){
			return ((Devise)d).nomDevise.equals(this.nomDevise);
		}
		return false;
	}
	
	public int hashCode(){
		return this.getNomDevise().hashCode();
	}
	
	public void setNomDevise(String nomDevise) {
		if (nomDevise==null || nomDevise.trim().length()==0) {
			throw new IllegalArgumentException("Le nom de la devise est vide !");
		}
		this.nomDevise = nomDevise;
	}
	
	public String getNomDevise() {
		return nomDevise;
	}
	
	
	
	/*
	 * Autres m�thodes
	 */
	
	public String toString() {

		return  this.nomDevise;
	}

	public void afficher() {

		System.out.println(this);
	}
	

	@Override
    public int compareTo(Devise o) {
		
		return this.nomDevise.compareTo(o.nomDevise);
	}
	
}