package td3;

import java.awt.List;

public interface InterfaceDAO {

	/**
     * Ajout un element
     * @param object
     * @return 
     */
    boolean add(Object object);
    /**
     * Actualitation d'un objet
     * @param object
     * @return 
     */
    boolean update(Object object);
    /**
     * Suprimer un element
     * @param object
     * @return 
     */
    boolean delete(Object object);
    /**
     * Filter by element
     * @param object
     * @return 
     */
    List filterDataList(Object object);
    
}
