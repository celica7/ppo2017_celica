package td3;

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serealisation {
 private static File root = new File(".");
 
   public static void sauvegarde(List<Portefeuille> listData) {
		File fichierSauvegarde = null;
		String routeDossierActuel;
		ObjectOutputStream out = null;
		try {
			//On obtient la route du dossier actuel
			routeDossierActuel = root.getCanonicalPath();
			fichierSauvegarde = new File(routeDossierActuel + "/sauvegarde.bin");
			out = new ObjectOutputStream(new FileOutputStream(fichierSauvegarde));
			out.writeObject(listData);
		} catch(Exception ex) {
			System.err.println(ex.getMessage());
		} finally {
			if (fichierSauvegarde != null) {
				try {
					out.close();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		}
	}
 
   
   @SuppressWarnings("unchecked")
	public static List<Portefeuille> getBackup(){
		File fichierBinaire = null;
		String route;
		ObjectInputStream input = null;
		List<Portefeuille> listPortefeuilles = null;
		try {
			route = root.getCanonicalPath();
			fichierBinaire = new File(route + "/sauvegarde.bin");
			input = new ObjectInputStream(new FileInputStream(fichierBinaire));
			listPortefeuilles = (ArrayList<Portefeuille>) input.readObject();
			input.close();
		} catch(IOException | ClassNotFoundException ex) {
			System.out.println("Il n'y a pas fichier sauvegarde.");
		}
		return listPortefeuilles;
	}
 
	
}
