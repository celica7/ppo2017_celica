package td3;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Portefeuille {

	/**
	 * Attribut de la classe
	 */
	
	HashMap<Devise, Double> hmap;
    private String nom;
   
	/**
	 * Constructeur par d�faut
	 */
	public Portefeuille() {
       this("America");
	}

	/**
	 * Constructeurs
	 */
    
	public Portefeuille(String nom) {
		this.setNom(nom);
		hmap = new HashMap<Devise, Double>();
	}
    
	/**
	 * M�thod pour ajouter une devise 
	 * @param nom Valeur String, nom de la devise.
	 * @param monto Valeur double, monto de la devise.
	 */
	public void addCartera(String nom, double monto){
		if(nom==null || nom.trim().length()==0 || monto<0){
			throw new IllegalArgumentException("V�rifier le nom et montat de la devise");
		}else{
			hmap.put(new Devise(nom), monto);
		}
	}
	
	/**
	 * M�thod pour montrer les devises
	 */
	public void montrerDevises(){
		Iterator it = hmap.entrySet().iterator();
		 
	    while (it.hasNext()) {
	        Map.Entry e = (Map.Entry)it.next();
	        System.out.println(e.getKey() + " Quantit�: " + e.getValue());
	   }
	}
	
	/**
	 * M�thod pour ajouter un montant � la devise
	 * @param nom Valeur String, C'est le nom de la devise
	 * @param monto Valeur double, C'est le montant
	 */
	public void ajouterMontant(String nom,double monto){
		if(nom==null || nom.trim().length()==0 || monto<0){
			throw new IllegalArgumentException("V�rifier le nom et montat de la devise");
		}else{
			hmap.put(new Devise(nom),hmap.get(new Devise(nom))+monto );
		}
	}
	
	/**
	 * M�thod pour retrait un montant � la devise
	 * @param nom Valeur String, C'est le nom de la devise
	 * @param monto Valeur double, C'est le montant
	 */
	public void retraitMontant(String nom,double monto){
		if(nom==null || nom.trim().length()==0 || monto<0){
			throw new IllegalArgumentException("V�rifier le nom et montat de la devise");
		}else{
			hmap.put(new Devise(nom),hmap.get(new Devise(nom))- monto );
		}
	}
	
	
    /*
     * M�thod d'acces au portefeuille
     */
	public void setNom(String nom) {
		if(nom==null || nom.trim().length()==0){
			throw new IllegalArgumentException("Veuilez �crir un nom au portefeuille");
		}else{
			this.nom = nom;
		}
		
	}

	public String getNom() {
		return nom;
	}

	public String toString() {
		return  this.nom;
	}
	
	
   
}
