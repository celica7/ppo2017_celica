package td3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DaoContenu {

	 /*
     * Variables de la classe
     */
	private Connection con;
    private PreparedStatement pst;
    private boolean status;
    private Portefeuille portefeuille;
    private Devise devise;
    
    //Singleton
    private static DaoContenu instance;
    private DaoContenu() {}
    public static DaoContenu getInstance() {
    if (instance==null) {
    instance = new DaoContenu();
    }
    return instance;
    }
    
    
    public boolean addContenu(Object object, Devise d, Double montant) {
		portefeuille = (Portefeuille) object;
		try {
            con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute addContenu ?,?,?;");
            pst.setString(1, portefeuille.getNom());
            pst.setString(2, d.getNomDevise());
            pst.setDouble(3, montant);
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur: " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Erreur: " + e);
            }
        }
        return status;
	}
    
    
    
    
    
    
    
    public boolean addMontant(Object object, Devise d, Double montant) {
		portefeuille = (Portefeuille) object;
		try {
            con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute addAjout ?,?,?;");
            pst.setString(1, portefeuille.getNom());
            pst.setString(2, d.getNomDevise());
            pst.setDouble(3, montant);
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur: " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Erreur: " + e);
            }
        }
        return status;
	}
    
    
    public boolean addRetrait(Object object, Devise d, Double montant) {
		portefeuille = (Portefeuille) object;
		try {
            con = Bdd.creeConnexion();
            pst = con.prepareStatement("execute addRetrait ?,?,?;");
            pst.setString(1, portefeuille.getNom());
            pst.setString(2, d.getNomDevise());
            pst.setDouble(3, montant);
            status = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Erreur: " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Erreur: " + e);
            }
        }
        return status;
	}
}
